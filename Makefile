###############################################################################
#
#  The CentOS Web Browser Default Page
#  Copyright (C) 2021 Alain Reguera Delgado
#
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option)
#  any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <https://www.gnu.org/licenses/>.
#
###############################################################################


#==============================================================================
# Visual identity definition
#==============================================================================
brands_dir = ../../../../Brands
website_dir = ../../../Webistes
typographies_dir = ../../../../Typographies
backgrounds_srcdir = ../../Packages/centos-logos/backgrounds

#==============================================================================
# Configuration
#==============================================================================
prefix = ../../Packages/centos-indexhtml

design_dir = .
design_locale_dir = $(design_dir)/PO
design_html_dir = $(design_dir)/HTML
design_pot_files := $(wildcard $(design_locale_dir)/??-??.pot)
design_po_files := $(foreach file,$(design_pot_files),$(subst .pot,.po,$(file)))
design_locale_codes := $(foreach file,$(design_pot_files),$(notdir $(subst .pot,,$(file))))

package_dirs = \
	$(prefix)/common \
	$(prefix)/common/img \
	$(prefix)/common/css

package_html_files := $(foreach code,$(design_locale_codes),$(prefix)/$(code)/index.html)
package_files = \
	$(prefix)/common/img/favicon.png \
	$(prefix)/common/img/logo.png \
	$(prefix)/common/img/motif.png \
	$(prefix)/common/img/centos_rhel_timeline.png \
	$(design_html_dir)/common/css/stylesheet.css \
	$(prefix)/common/css/centos.bootstrap.min.css \
	$(prefix)/common/css/centos-indexhtml.min.css \
	$(prefix)/index.html

.PHONY: all
all: $(package_dirs) $(package_files)
	for lang in $(design_locale_codes); do $(MAKE) lang=$${lang} $(prefix)/$${lang}/index.html;done
	wkhtmltoimage --enable-local-file-access --disable-smart-width --width 1440 $(prefix)/en-US/index.html screenshot.png || true
	pngquant -f --ext .png screenshot.png

#==============================================================================
# Website theme
#==============================================================================
# The website theme (jekyll-theme-centos) provides all the images and css files
# we need to provide a consistent presentation page, so let's reuse them.
$(website_dir)/jekyll-theme-centos/_site/assets/img/favicon.png:
	$(MAKE) -C $(website_dir) -f jekyll-theme-centos.Makefile

$(website_dir)/jekyll-theme-centos/_site/assets/css/centos.bootstrap.min.css:
	$(MAKE) -C $(website_dir) -f jekyll-theme-centos.Makefile

$(website_dir)/jekyll-theme-centos/_site/assets/img/motif.png:
	$(MAKE) -C $(website_dir) -f jekyll-theme-centos.Makefile

$(website_dir)/jekyll-theme-centos/_site/assets/img/logo.png:
	$(MAKE) -C $(website_dir) -f jekyll-theme-centos.Makefile

$(website_dir)/jekyll-theme-centos/_site/assets/fonts:
	$(MAKE) -C $(website_dir) -f jekyll-theme-centos.Makefile

#==============================================================================
# Template expansion
#==============================================================================
# The template expansion allows us to reuse the website configuration at
# _data/centos/*.yml. So, we always provide the same information (e.g., links,
# social media, project description, etc.)
$(design_html_dir)/index.l18n.html: $(design_html_dir)/index.l18n.html.j2
	python render.py $< $@

$(design_html_dir)/index.html: $(design_html_dir)/index.html.j2
	python render.py $< $@

#==============================================================================
# Localization
#==============================================================================
.PHONY: locale
locale:
	if [[ -z $$lang ]]; then \
		for lang in $(design_locale_codes); do $(MAKE) lang=$${lang} $(design_locale_dir)/$${lang}.po;done; \
	else \
		$(MAKE) lang=$${lang} $(design_locale_dir)/$${lang}.po; \
	fi

$(design_locale_dir)/$(lang).pot: $(design_html_dir)/index.l18n.html
	html2po -P --input $< --output $@.tmp
	if [[ ! -f $@ ]];then \
		msginit -i $@.tmp -o $@ -l $(lang) --no-translator --width=80; \
	else \
		msgmerge -U $@ $@.tmp; \
	fi
	rm $@.tmp

$(design_locale_dir)/$(lang).po: $(design_locale_dir)/$(lang).pot
	if [[ ! -f $@ ]];then \
		msginit -i $< -o $@ -l $(lang) --no-translator --width=80; \
	else \
		msgmerge -U $@ $<; \
	fi

#==============================================================================
# 
#==============================================================================
.PHONY: $(package_dirs)
$(package_dirs):
	test -d $@ || mkdir $@

$(prefix)/common/img/motif.png: $(website_dir)/jekyll-theme-centos/_site/assets/img/motif.png
	install $< $@

$(prefix)/common/img/logo.png: $(website_dir)/jekyll-theme-centos/_site/assets/img/logo.png
	install $< $@

$(prefix)/common/img/favicon.png: $(website_dir)/jekyll-theme-centos/_site/assets/img/favicon.png
	install $< $@

$(prefix)/common/img/centos_rhel_timeline.png: $(design_html_dir)/common/img/centos_rhel_timeline.png
	install $< $@

$(prefix)/common/css/centos.bootstrap.min.css: $(website_dir)/jekyll-theme-centos/_site/assets/css/centos.bootstrap.min.css
	install $< $@

$(design_dir)/HTML/common/css/stylesheet.css: $(design_html_dir)/common/css/stylesheet.css.j2 \
	$(design_dir)/HTML/common/fonts/base64
	python render.py $< $@

$(prefix)/common/css/centos-indexhtml.min.css: $(design_html_dir)/common/css/stylesheet.css
	yuicompressor --type css -o $@ $<

.PHONY: $(design_dir)/HTML/common/fonts/base64
$(design_dir)/HTML/common/fonts/base64:
	test -d $(@D) || mkdir $(@D)
	for FONT in $(typographies_dir)/Montserrat/fonts/webfonts/*; \
		do base64 --wrap 0 $${FONT} > $(@D)/$$(basename $${FONT}.base64); \
	done; \
	for FONT in $(typographies_dir)/Fontawesome/webfonts/*; \
		do base64 --wrap 0 $${FONT} > $(@D)/$$(basename $${FONT}.base64); \
	done; \
	for FONT in $(typographies_dir)/Overpass/fonts/webfonts_mono/*; \
		do base64 --wrap 0 $${FONT} > $(@D)/$$(basename $${FONT}.base64); \
	done

$(prefix)/$(lang)/index.html: $(design_locale_dir)/$(lang).po $(design_html_dir)/index.l18n.html
	test -d $(@D) || mkdir $(@D)
	po2html -i $<  -t $(design_html_dir)/index.l18n.html -o $@
	tidy -config $(design_dir)/tidy.conf -m $@

$(prefix)/index.html: $(design_html_dir)/index.html
	tidy -config $(design_dir)/tidy.conf -o $@ $<

.PHONY: clean
clean:
	@rm -r $(design_html_dir)/common/fonts
	@rm -r $(design_html_dir)/common/css/stylesheet.css
	@rm -r $(design_html_dir)/index.l18n.html
	@rm -r $(design_html_dir)/index.html
	@rm -r $(prefix)/*
