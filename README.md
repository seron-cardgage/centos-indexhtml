# centos-indexhtml

This project is a community effort to develop the files necessary to build the
[centos-indexhtml](https://github.com/centos/centos-indexhtml) package.

## Screenshot

![centos-indexhtml page screenshot](screenshot.png)

# Requisites

```
sudo dnf -y install translate-toolkit yuicompressor tidy wkhtmltopdf
```

# Localization

The web browser's default page uses a javascript in the main index.html file
to automatically redirect the user to the language-specific directory where the
translated index.html file is in. This way it is possible to show the browser's
default page in the specific locale the CentOS distribution was installed for.

To localize the final index.html file, do the following:

1. Edit the `HTML/index.tpl.html` file. This file controls the web browser's
   default page graphic design and the content itself. Translators don't need to
   touch this file. This file is for graphic designers and CentOS authorities
   setting the page content itself in English language.

2. Edit/Create the language-specific PO files. To do this you need to run the
   `make` command with the `locale` argument. Consider an example where your are
   producing language-specific `index.html` file for Spanish (es-ES). In this
   case you, run:

   ```
   make locale lang=es-ES
   ```

   This command will create the file `PO/es-ES.pot` and `PO/es-ES.po`. From these
   files, you, as translator only need to edit the one ending in `.po`. Here is
   where you do all the translation stuff with your favorite text editor.
  
3. Build the localized index.html files. To do this, run the `make` command:

   ```
   make build
   ```