msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-10 12:10-0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: HTML/index.l18n.html%2Bhtml[lang]:2-1
msgid "en-US"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.head.title:4-5
msgctxt "HTML/index.l18n.html+html.head.title:4-5"
msgid "Welcome to CentOS Stream 9"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.header.div.a.img[alt]:14-69
msgid "CentOS Project"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.header.h1:15-7
msgctxt "HTML/index.l18n.html+html.body.header.h1:15-7"
msgid "Welcome to CentOS Stream 9"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.header.p:16-7
msgid ""
"CentOS Stream is a continuous-delivery distribution serving as the next "
"point-release of RHEL."
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.h2:29-7
msgid "CentOS Stream is Continuous"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.p:31-7
msgid ""
"Before a package is formally introduced to CentOS Stream, it undergoes a "
"battery of tests and checks—both automated and manual—to ensure it meets the "
"stringent standards for packages to be included in RHEL. Updates posted to "
"Stream are identical to those posted to the unreleased minor version of "
"RHEL. The aim? For CentOS Stream to be as fundamentally stable as RHEL "
"itself."
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.p:38-7
msgid ""
"To achieve this stability, each major release of Stream starts from a stable "
"release of Fedora—In CentOS Stream 9, this begins with Fedora 34, which is "
"the same code base from which RHEL 9 is built. As updated packages pass "
"testing and meet standards for stability, they are pushed into CentOS Stream "
"as well as the nightly build of RHEL. What you see in CentOS Stream now is "
"what RHEL will look like in the future."
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.figure.img[alt]:46-7
msgid "How Fedora becomes CentOS; how CentOS becomes RHEL"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.figure.figcaption:47-7
msgid ""
"OS Versions as they move from Fedora to CentOS Stream to Red Hat Enterprise "
"Linux."
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.p:50-7
msgid "CentOS Stream is made for you to make it what you want it to be."
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.h2:52-7
msgid "CentOS Stream is Community"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.p:54-7
msgid ""
"Although many CentOS Stream contributions derive from Red Hat employees, "
"CentOS Stream thrives on community support. CentOS Stream is a stable, "
"reliable platform for open source communities to expand upon, allowing "
"people from all areas and backgrounds to collaborate in an open environment."
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.p:60-7
msgid ""
"As CentOS Stream ultimately becomes RHEL, contributors also have an "
"opportunity for their work to influence future builds of RHEL; this makes "
"CentOS Stream an ideal environment for creativity and forward-thinking."
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.h2:65-7
msgid "CentOS Stream Moves Forward"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.p:67-7
msgid ""
"CentOS Stream offers a unique blend of progress and stability—rather than "
"delivering most updates in a large minor release batch, CentOS Stream "
"receives stable updates as soon as they are available. This, in turn, "
"provides an ideal distribution where stability is king and doesn't come at "
"the cost of time."
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.h2:73-7
msgid "Getting CentOS Stream"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.p:75-7
msgid ""
"CentOS Stream can be downloaded as <a href=\"https://www.centos.org/centos-"
"stream/\">an ISO</a> from our mirrors and is compatible with x86-64, ARM64 "
"(aarch64), s390x, and IBM Power (ppc64le) architectures."
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.h2:80-7
msgid "Contribute to CentOS Stream"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.p:82-7
msgid ""
"Community is at the heart of The CentOS Project, and there are many ways you "
"can contribute. A list of areas where you can contribute is available on the "
"<a href=\"https://wiki.centos.org/Contribute\">CentOS Wiki</a>."
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.p:87-7
msgid ""
"Because CentOS Stream is upstream of RHEL, it offers an ideal environment "
"for testing applications which are designed be deployed in RHEL. We welcome "
"and encourage contributors from all backgrounds—especially those developing "
"for the post-RHEL production stream—to use CentOS Stream to build, test, and "
"deploy the applications that are special to you and to the greater Linux "
"community."
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.p:94-7
msgid ""
"You can also contribute by joining (or creating) a Special Interest Group "
"(SIG) in an area of your interest. Visit the <a href=\"https://wiki.centos."
"org/SpecialInterestGroup/\">CentOS Wiki</a> to learn more."
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.h2:99-7
msgid "Learn More"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.main.article.p:101-7
msgid ""
"To learn more about CentOS Stream 9, visit the <a href=\"https://www.centos."
"org/stream9\">CentOS Website</a>."
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.h6:119-11
msgid "About"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.ul.li:122-13
msgid "About CentOS"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.ul.li:124-13
msgid "Frequently Asked Questions (FAQs)"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.ul.li:126-13
msgid "Special Interest Groups (SIGs)"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.ul.li:128-13
msgid "CentOS Variants"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.ul.li:130-13
msgid "Governance"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.ul.li:132-13
msgid "Code of Conduct"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.h6:137-11
msgid "Community"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.ul.li:140-13
msgid "Contribute"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.ul.li:142-13
msgid "Forums"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.ul.li:144-13
msgid "Mailing Lists"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.ul.li:146-13
msgid "IRC"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.ul.li:148-13
msgid "Calendar &amp; IRC Meeting List"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.ul.li:150-13
msgid "Planet"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.ul.li:152-13
msgid "Submit Bug"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.h2:157-11
msgid "The CentOS Project"
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.p:158-11
msgid ""
"Community-driven free software effort focused around the goal of providing a "
"rich base platform for open source communities to build upon."
msgstr ""

#: HTML/index.l18n.html%2Bhtml.body.footer.div.div.section.p:177-11
msgid ""
"Copyright © 2021 The CentOS Project | <a href=\"https://www.centos.org/legal"
"\">Legal</a> | <a href=\"https://www.centos.org/legal/privacy\">Privacy</a> "
"| <a href=\"https://git.centos.org/centos/centos.org\">Site source</a>"
msgstr ""
